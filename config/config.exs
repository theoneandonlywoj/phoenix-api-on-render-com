# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :phoenix_api,
  namespace: PhoenixAPI,
  ecto_repos: [PhoenixAPI.Repo]

# Configures the endpoint
config :phoenix_api, PhoenixAPIWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "g0BiMQXgfGKVw9068SPdHOd9vZ1c2oCfWaRouJ4mOqeeZOoLihi3wifeELmOfG41",
  render_errors: [view: PhoenixAPIWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: PhoenixAPI.PubSub,
  live_view: [signing_salt: "eCfmOjBH"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
