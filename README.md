# PhoenixAPI
Created a new project as follows:
```bash
mix phx.new phoenix_api --app phoenix_api --module PhoenixAPI --no-html --no-webpack
```
Change the database info in `config/dev.exs`
Create the database
```bash
mix ecto.create
```
Migrate
```bash
mix ecto.migrate
```
Testing Development Server
```bash
mix phx.server
```
In `/config/prod.exs` delete line 14 and replace line 13:
```elixir
config :phoenix_api, PhoenixAPIWeb.Endpoint,
  url: [host: "example.com", port: 80],
  cache_static_manifest: "priv/static/cache_manifest.json"
```
to:
```elixir
config :phoenix_api, PhoenixAPIWeb.Endpoint,
  url: [host: System.get_env("RENDER_EXTERNAL_HOSTNAME") || "localhost", port: 80]
```
Comment out the last line in `/config/prod.exs`:
```elixir
# import_config "prod.secret.exs"
```
Rename `/config/prod.secret.exs` to `/config/releases.exs`.
Uncomment line 39 in `/config/releases.ex`:
```elixir
config :phoenix_api, PhoenixAPIWeb.Endpoint, server: true
```
Change line 5 in `/config/releases.exs` (from `use Mix.Config` to `import Config`).
Add build file `build.sh` in the root of the project.
```bash
#!/usr/bin/env bash
# exit on error
set -o errexit

# Initial setup
mix deps.get --only prod
MIX_ENV=prod mix compile

# Build the release and overwrite the existing release directory
MIX_ENV=prod mix release --overwrite
```

Make the file being executable before uploading it to Gitlab / Github:
```bash
chmod a+x build.sh
```

Test the setup
```bash
SECRET_KEY_BASE=`mix phx.gen.secret` DATABASE_URL=postgres://ergo_decoded_dev:jCWxKyTOAsFzR51QvAkVZaCcTXjCVDQk@postgres.render.com/ergo_decoded_dev _build/prod/rel/phoenix_api/bin/phoenix_api start
```

# Creating a new App
https://render.com/docs/deploy-phoenix#deploy-to-render

# Adding migrations
https://hexdocs.pm/phoenix/releases.html

# Migrate (don't forget to re-build)
Build
```bash
MIX_ENV=prod mix release --overwrite
```
Migrate
```bash
SECRET_KEY_BASE=`mix phx.gen.secret` DATABASE_URL=postgres://ergo_decoded_dev:jCWxKyTOAsFzR51QvAkVZaCcTXjCVDQk@postgres.render.com/ergo_decoded_dev _build/prod/rel/phoenix_api/bin/phoenix_api eval "PhoenixAPI.Release.migrate"
```

# Issues with SSL connection for migrations
Add `Application.ensure_all_started(:ssl)` on the top of the `migrate/0` function in `lib/phoenix_api/releases.ex`:
```elixir
def migrate do
  Application.ensure_all_started(:ssl)
  load_app()

  for repo <- repos() do
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
  end
end
```